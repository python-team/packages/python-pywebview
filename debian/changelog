python-pywebview (5.0.5+dfsg-2) unstable; urgency=medium

  * Team upload.
  * d/control: build-dep on setuptools-scm to fix wrong "0.0.0" version.
    (LP: #2063176)
  * Mark Debian-specific patches as not needing forwarding upstream.
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 26 Apr 2024 18:50:20 +0200

python-pywebview (5.0.5+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Add patch to avoid dependency on proxy_tools which is not in Debian
    (Closes: #1063096)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 01 Apr 2024 14:51:18 -0400

python-pywebview (4.4.1+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Depend on webkit2gtk 4.1 instead of 4.0
  * Drop alternate dependencies on Python bindings for qtwebengine-opensource-src
    since it is not supported by the Debian Security Team
  * Add pybuild-plugin-pyproject to Build-Depends
  * Add patch to adjust build for the dlls we exclude
  * Set upstream metadata fields: Security-Contact
  * Update standards version to 4.6.2, no changes needed

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 24 Jan 2024 17:36:15 -0500

python-pywebview (3.3.5+dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.
  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 14 Dec 2022 20:49:02 +0000

python-pywebview (3.3.5+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Francisco Vilmar Cardoso Ruviaro ]
  * New upstream version 3.3.5+dfsg.
  * debian/control:
      - Bumped DH level to 13.
      - Bumped Standards-Version to 4.5.0.
  * debian/copyright:
      - Added 'docs/.vuepress/public/2.4/assets/js' in Files-Excluded field.
      - Added new rights.
      - Updated packaging copyright.
      - Updated upstream copyright years.
  * debian/upstream/metadata: added Repository and Repository-Browse fields.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Sun, 11 Oct 2020 18:16:59 +0000

python-pywebview (2.3+dfsg-1) unstable; urgency=low

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Remove trailing whitespaces.

  [ Michael Fladischer ]
  * New upstream release.
  * Clean up files in pywebview.egg-info to allow two builds in a row.
  * Install README.md in binary package.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Add debian/gbp.conf.
  * Use PYBUILD customization to skip tests.
  * Remove python3-pyqt5 from Build-Depends as tests are not run.
  * Use debhelper-compat and bump to version 12.
  * Remove python3-django from Depends as it is never imported.
  * Add CNF dependenies so all three possible backends (Qt5 WebEngine,
    Qt5 WebKit, GTK WebKit) are accepted.
  * Rename MIT license to Expat.

 -- Michael Fladischer <fladi@debian.org>  Sun, 17 Feb 2019 09:33:09 +0100

python-pywebview (2.2.1+dfsg-4) unstable; urgency=medium

  * Add Conflict/Replace

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 02 Jan 2019 19:42:48 +0100

python-pywebview (2.2.1+dfsg-3) unstable; urgency=medium

  * Fix copyright syntax
  * Rename binary package according to module name

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 02 Jan 2019 18:25:25 +0100

python-pywebview (2.2.1+dfsg-2) unstable; urgency=medium

  * Add comment for disabled tests
  * Add missing copyrights.
    Thanks to Chris Lamb (Closes: #917771)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 02 Jan 2019 17:49:46 +0100

python-pywebview (2.2.1+dfsg-1) unstable; urgency=low

  * Initial release. Closes: #917621

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 23 Dec 2018 16:36:22 +0100
