Source: python-pywebview
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Jochen Sprickerhof <jspricke@debian.org>,
Build-Depends:
 debhelper-compat (=13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
 python3-setuptools-scm,
 pybuild-plugin-pyproject
Standards-Version: 4.6.2
Homepage: https://github.com/r0x0r/pywebview
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pywebview.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pywebview
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python3-webview
Architecture: all
Multi-Arch: foreign
Depends:
 gir1.2-webkit2-4.1,
 python3-gi,
 python3-gi-cairo,
 ${misc:Depends} ,
 ${python3:Depends},
Conflicts:
 python3-pywebview,
Replaces:
 python3-pywebview,
Description: Build GUI for your Python program with JavaScript, HTML, and CSS
 pywebview is a lightweight cross-platform wrapper around a webview component
 that allows on to display HTML content in its own native GUI window. It gives
 you power of web technologies in your desktop application, hiding the fact
 that GUI is browser based. You can use pywebview either with a lightweight web
 framework like Flask or Bottle or on its own with a two way bridge between
 Python and DOM.
 .
 pywebview uses native GUI for creating a web component window: WinForms on
 Windows, Cocoa on macOS and QT or GTK+ on Linux. If you choose to freeze your
 application, pywebview does not bundle a heavy GUI toolkit or web renderer
 with it keeping the executable size small. pywebview is compatible with both
 Python 2 and 3.
